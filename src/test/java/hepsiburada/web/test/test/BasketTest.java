package hepsiburada.web.test.test;

import hepsiburada.web.test.base.BaseTest;
import hepsiburada.web.test.page.BasketPage;
import hepsiburada.web.test.page.MainPage;
import org.junit.Test;

public class BasketTest extends BaseTest {

    private String email = "hoxaxutuwa@mailhex.com";
    private String password = "Test123!";
    private String productName = "iPhone 8";

    @Test
    public void addItemToBasketTestAlreadyLogin() {
        String firstMerchant = new MainPage(driver).callLoginPage().successLogin(email, password).searchProduct(productName)
                .chooseProductFromList(1).addBasketChoosenProduct();

        String secondMerchant = new BasketPage(driver).goBackSearchResult().chooseProductFromList(2).addBasketChoosenProduct();

        new BasketPage(driver).validateProducts(productName, firstMerchant, secondMerchant);
    }

    @Test
    public void addItemToBasketTestAlreadyNotLogin() {
        String firstMerchant = new MainPage(driver).searchProduct(productName).chooseProductFromList(1).addBasketChoosenProduct();

        String secondMerchant = new BasketPage(driver).goBackSearchResult().chooseProductFromList(2).addBasketChoosenProduct();

        new BasketPage(driver).validateProducts(productName,firstMerchant,secondMerchant);
    }
}
