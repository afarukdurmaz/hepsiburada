package hepsiburada.web.test.base;

import hepsiburada.web.test.page.LoginPage;
import hepsiburada.web.test.util.BasePageUtil;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BasePage extends BasePageUtil {
    public BasePage(WebDriver driver) {
        super(driver);
    }

    public LoginPage callLoginPage() {
        mouseMove(getElementById("myAccount"));
        clickObjectById("login");
        Assert.assertTrue("Login sayfası görüntülenmedi !",
                isElementPresentAndDisplay(driver, By.cssSelector(".login-box")));
        return new LoginPage(driver);
    }
}
