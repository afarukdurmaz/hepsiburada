package hepsiburada.web.test.page;

import hepsiburada.web.test.base.BasePage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public MainPage successLogin(String email, String password) {
        setObjectById("email", email);
        setObjectById("password", password);
        clickObjectByCss(".btn-login-submit");
        Assert.assertTrue("Login Olma Islemi Basarisiz !!!", getElementByCssSelector(".user-name").getText().equals("Test Test"));
        return new MainPage(driver);
    }
}
