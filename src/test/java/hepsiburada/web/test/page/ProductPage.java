package hepsiburada.web.test.page;

import hepsiburada.web.test.base.BasePage;
import org.openqa.selenium.WebDriver;

public class ProductPage extends BasePage {
    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public String addBasketChoosenProduct() {
        String merchant = getElementByCssSelector(".seller span a").getText();
        clickObjectById("addToCart");
        return merchant;
    }
}
