package hepsiburada.web.test.page;

import hepsiburada.web.test.base.BasePage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class BasketPage extends BasePage {
    public BasketPage(WebDriver driver) {
        super(driver);
    }

    public MainPage goBackSearchResult(){
        driver.navigate().back();
        sleep(5000);
        driver.navigate().back();
        sleep(5000);
        System.out.println("Arama sonuclarina geri gidildi !");
        return new MainPage(driver);
    }

    public BasketPage validateProducts(String productName, String firstMerchant,String secondMerchant) {
        List<WebElement> productListOnBasket = getElementsByCss(".product-detail .product-name a");
        for (WebElement webElement :
                productListOnBasket) {
            if (!webElement.getText().contains(productName)) {
                Assert.assertTrue("Sepete eklenen urun ile eklenmek istenen urun farkli geldi !!!", false);
            }
        }
        System.out.println("First Merchant = " + firstMerchant);
        System.out.println("----------------------------------");
        System.out.println("Second Merchant = " + secondMerchant);
        Assert.assertTrue("Iki farkli merchanttan urun sepete ekleme islemi basarsiz oldu! ",!firstMerchant.equals(secondMerchant));
        return this;
    }
}
