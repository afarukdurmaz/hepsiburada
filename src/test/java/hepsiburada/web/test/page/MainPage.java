package hepsiburada.web.test.page;

import hepsiburada.web.test.base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class MainPage extends BasePage {
    public MainPage(WebDriver driver) {
        super(driver);
    }

    public MainPage searchProduct(String productName){
        setObjectById("productSearch", productName);
        clickObjectById("buttonProductSearch");
        return this;
    }

    public ProductPage chooseProductFromList(int index){
        List<WebElement> productList = getElementsByCss(".search-item");
        productList.get(index - 1).click();
        return new ProductPage(driver);
    }

}
