package hepsiburada.web.test.util;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BasePageUtil<T> {

	protected WebDriver driver;

	private final static int WAIT_TIME = 30;

	public BasePageUtil(WebDriver driver) {
		this.driver = driver;
	}

	public void mouseMove(WebElement webElement) {
		new Actions(driver).moveToElement(webElement).perform();
	}

	private WebElement getObjectByClickable(By by) {
		WebElement webElement = (new WebDriverWait(driver, WAIT_TIME))
				.until(ExpectedConditions.elementToBeClickable(by));
		mouseMove(webElement);
		return webElement;
	}

	private WebElement getObjectByPresentOfElement(By by) {
		WebElement webElement = (new WebDriverWait(driver, WAIT_TIME))
				.until(ExpectedConditions.visibilityOfElementLocated(by));
		mouseMove(webElement);
		return webElement;
	}

	protected WebElement setObjectBy(By by, String value) {
		WebElement webElement = getObjectByPresentOfElement(by);
		webElement.clear();
		webElement.sendKeys(value);
		return webElement;
	}

	protected WebElement clickObjectBy(By by) {
		WebElement webElement = getObjectByClickable(by);
		webElement.click();
		return webElement;
	}

	private List<WebElement> getObjectByPresentOfElementList(By by) {
		List<WebElement> webElements = (new WebDriverWait(driver, WAIT_TIME))
				.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
		return webElements;
	}


	protected WebElement setObjectById(String id, String value) {

		return setObjectBy(By.id(id), value);
	}

	protected WebElement clickObjectById(String id) {

	    return clickObjectBy(By.id(id));
	}

	protected WebElement clickObjectByCss(String css) {

	    return clickObjectBy(By.cssSelector(css));
	}

	public WebElement getElementById(String id) {

	    return getObjectByPresentOfElement(By.id(id));
	}

	public WebElement getElementByCssSelector(String css) {

	    return getObjectByPresentOfElement(By.cssSelector(css));
	}

	public List<WebElement> getElementsBy(By by) {

	    return getObjectByPresentOfElementList(by);
	}

	public List<WebElement> getElementsByCss(String css) {

	    return getElementsBy(By.cssSelector(css));
	}


	public void sleep(long time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
		}
	}


	public static boolean isElementPresentAndDisplay(WebDriver driver, By by) {
		try {
			return driver.findElement(by).isDisplayed();
		} catch (NoSuchElementException e) {
			return false;
		}
	}

}
